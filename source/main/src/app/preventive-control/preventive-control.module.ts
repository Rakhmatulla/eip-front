import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PreventiveControlComponent} from './preventive-control.component';
import {PreventiveControlRoutingModule} from "./preventive-control-routing.module";
import {MatTabsModule} from "@angular/material/tabs";
import {MatIconModule} from "@angular/material/icon";
import {MailingComponent} from './mailing/mailing.component';
import {SettingsComponent} from './settings/settings.component';
import {ReportComponent} from './report/report.component';
import {LogComponent} from './log/log.component';
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatRippleModule} from "@angular/material/core";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatTooltipModule} from "@angular/material/tooltip";
import {FormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {OwlDateTimeModule, OwlNativeDateTimeModule} from "ng-pick-datetime";
import {MatSelectModule} from "@angular/material/select";
import {ScheduleViolationService} from "./services/schedule-violation.service";
import {RespondentNotificationService} from "./services/respondent-notification.service";
import {BusinessLogService} from "./services/business-log.service";
import {MessageService} from "./services/message.service";
import {FileService} from "./services/file.service";
import {RespondentNotificationFormComponent} from './respondent-notification-form/respondent-notification-form.component';
import {MatCardModule} from "@angular/material/card";
import {ReportDialogComponent} from './report/report-dialog/report-dialog.component';
import {ReportService} from "./services/report.service";
import { MailingAttachConfigComponent } from './mailing-attach-config/mailing-attach-config.component';
import {MatSidenavModule} from "@angular/material/sidenav";


@NgModule({
  declarations: [
    PreventiveControlComponent,
    MailingComponent,
    SettingsComponent,
    ReportComponent,
    LogComponent,
    RespondentNotificationFormComponent,
    ReportDialogComponent,
    MailingAttachConfigComponent
  ],
    imports: [
        CommonModule,
        PreventiveControlRoutingModule,
        MatTabsModule,
        MatIconModule,
        MatButtonModule,
        MatTableModule,
        MatSortModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatPaginatorModule,
        MatDialogModule,
        MatRippleModule,
        MatSnackBarModule,
        MatTooltipModule,
        // MaterialModule,
        FormsModule,
        SharedModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        MatSelectModule,
        MatCardModule,
        MatSidenavModule
    ],
  providers: [ScheduleViolationService, RespondentNotificationService, BusinessLogService, MessageService, FileService, ReportService
  ]
})
export class PreventiveControlModule {
}
