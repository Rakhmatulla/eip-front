export class ReportGenerationModel {
  dateBefore: any;
  dateAfter: any;
  selectedStatus: string[];
  orgBIN: string;
  statusEnabled: boolean;
  binEnabled: boolean;
  svId: string;
}
