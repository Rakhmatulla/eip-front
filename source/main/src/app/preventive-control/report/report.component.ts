import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ScheduleViolation} from "../model/ScheduleViolation";
import {RespondentNotification} from "../model/RespondentNotification";
import {DataSource, SelectionModel} from "@angular/cdk/collections";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ScheduleViolationService} from "../services/schedule-violation.service";
import {RespondentNotificationService} from "../services/respondent-notification.service";
import {RespondentNotificationFormComponent} from "../respondent-notification-form/respondent-notification-form.component";
import {ReportDialogComponent} from "./report-dialog/report-dialog.component";

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  selection = new SelectionModel<RespondentNotification>(true, []);
  scheduleViolationColumns = ["formName", 'periodName', 'sendDate'];
  scheduleViolations: ScheduleViolation[] = [];
  respondentNotificationColumns = ['select', 'katoCode', 'binIin', 'respondentName', 'email', 'telefonStr', 'serviceStatus', 'desc', 'updated', 'attachment'];
  respondentNotifications: RespondentNotification[] = [];
  scheduleViolationLenght = 0;
  scheduleViolationIndex = 0;
  scheduleViolationSize = 5;
  respondentNotificationLength = 0;
  respondentNotificationIndex = 0;
  respondentNotificationSize = 5;
  selectedScheduleViolationId = '';
  selectedScheduleViolation: any;
  showDeletedMode = false;

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
              public scheduleViolationService: ScheduleViolationService,
              public respondentNotificationService: RespondentNotificationService,
              private snackBar: MatSnackBar) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild("filter", {static: true}) filter: ElementRef;

  ngOnInit(): void {
    this.getScheduleViolationPageable();
  }

  getScheduleViolationPageable(event?: PageEvent) {
    let params = '';

    if (event) {
      params = 'page=' + event.pageIndex + '&size=' + event.pageSize;
    } else {
      this.scheduleViolationLenght = 0;
      this.scheduleViolationIndex = 0;
      this.scheduleViolationSize = 5;
      params = 'page=' + this.scheduleViolationIndex + '&size=' + this.scheduleViolationSize;
    }
    this.scheduleViolationService.getAllPageable(params).subscribe(res => {
      this.scheduleViolations = res.content;
      this.scheduleViolationIndex = res.page;
      this.scheduleViolationSize = res.size;
      this.scheduleViolationLenght = res.totalElements;
    });
  }

  getRespondentNotificationPageable(event?: PageEvent) {
    let params = '';
    if (event) {
      params = 'page=' + event.pageIndex + '&size=' + event.pageSize + "&svId=" + this.selectedScheduleViolationId;
    } else {
      this.respondentNotificationLength = 0;
      this.respondentNotificationIndex = 0;
      this.respondentNotificationSize = 5;
      params = 'page=' + this.respondentNotificationIndex + '&size=' +
        this.respondentNotificationSize + "&svId=" + this.selectedScheduleViolationId;
    }
    this.respondentNotificationService.getAllPageable(params).subscribe(res => {
      this.respondentNotifications = res.content;
      this.respondentNotificationIndex = res.page;
      this.respondentNotificationSize = res.size;
      this.respondentNotificationLength = res.totalElements;
    });
  }

  getSentRespondentNotifications(row: any) {
    this.selectedScheduleViolationId = row.id;
    this.selectedScheduleViolation = row;
    let params = '';
    params = 'page=' + this.respondentNotificationIndex + '&size=' + this.respondentNotificationSize + '&svId=' + this.selectedScheduleViolationId;
    this.respondentNotificationService.showSent(this.selectedScheduleViolationId).subscribe(res => {
      this.respondentNotifications = res.content;
      this.respondentNotificationIndex = res.number;
      this.respondentNotificationSize = res.size;
      this.respondentNotificationLength = res.totalElements;
    });
  }

  // public loadData() {
  //   this.exampleDatabase = new DoctorsService(this.httpClient);
  //   this.periodsDataSource = new ExampleDataSource(
  //     this.exampleDatabase,
  //     this.paginator,
  //     this.sort
  //   );
  //   fromEvent(this.filter.nativeElement, "keyup").subscribe(() => {
  //     if (!this.periodsDataSource) {
  //       return;
  //     }
  //     this.periodsDataSource.filter = this.filter.nativeElement.value;
  //   });
  // }

  removeSelectedRows() {

  }

  addNew() {

  }

  refresh() {

  }

  viewDetail(row) {

  }

  selectDetail(i, row) {

  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.respondentNotifications.forEach((row) =>
        this.selection.select(row)
      );
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.respondentNotifications.length;
    return numSelected === numRows;
  }

  customCut(obj: string, ln: number): string {
    if (obj != null && obj.length > ln) {
      return obj.substring(0, ln) + '......';
    }
    if (obj === null || obj.length === null) {
      return '-';
    }
    return obj;
  }

  showMessageListByBinIin(id: string, binIin: string
  ) {
    this.respondentNotificationService.getByIdAndBinIin(id, binIin).subscribe(value => {
      console.log(value);
      this.respondentNotifications = value;
    });
  }

  editCall(row) {


  }

  reportSelectionDiaqlog() {

    const dialogRef = this.dialog.open(ReportDialogComponent, {
      data: {
        scheduleViolationId: this.selectedScheduleViolationId,
        selectedScheduleViolation: this.selectedScheduleViolation
      },
      direction: 'ltr',
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
    });


  }
}


