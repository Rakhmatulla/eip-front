import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ReportGenerationModel} from "../report-generation.model";
import {ReportService} from "../../services/report.service";

@Component({
  selector: 'app-report-dialog',
  templateUrl: './report-dialog.component.html',
  styleUrls: ['./report-dialog.component.scss']
})
export class ReportDialogComponent implements OnInit {
  action: string;
  scheduleViolationId: string;
  statusList: string[] = ['ERROR', 'SUCCESS'];

  selectedScheduleViolation: any;

  reportGeneration: ReportGenerationModel;

  constructor(
    public dialogRef: MatDialogRef<ReportDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public reportService: ReportService
  ) {
    console.log('data', data);
    this.scheduleViolationId = data.scheduleViolationId;
    this.selectedScheduleViolation = data.selectedScheduleViolation;
  }

  ngOnInit(): void {
    this.reportGeneration = new ReportGenerationModel();
    this.reportGeneration.binEnabled = false;
    this.reportGeneration.statusEnabled = false;
    this.reportGeneration.svId = this.scheduleViolationId;
  }


  generate() {
    console.log('reportGeneration', this.reportGeneration);
    this.reportService.generateMailingStatusReport(this.reportGeneration).subscribe(response => {
      console.log('response', response);
      const file = new Blob([response], {type: 'application/pdf'});
      const fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    });
    // this.dialogRef.close();
  }
}
