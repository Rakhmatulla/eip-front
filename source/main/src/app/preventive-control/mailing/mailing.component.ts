import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DataSource, SelectionModel} from "@angular/cdk/collections";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {BehaviorSubject, fromEvent, merge, Observable} from "rxjs";
import {map} from "rxjs/operators";
import {ScheduleViolationService} from "../services/schedule-violation.service";
import {RespondentNotificationService} from "../services/respondent-notification.service";
import {ScheduleViolation} from "../model/ScheduleViolation";
import {RespondentNotification} from "../model/RespondentNotification";
import {AuthService} from "../../core/service/auth.service";
import {MessageService} from "../services/message.service";
import {FormComponent} from "../../contacts/form/form.component";
import {RespondentNotificationFormComponent} from "../respondent-notification-form/respondent-notification-form.component";

@Component({
  selector: 'app-mailing',
  templateUrl: './mailing.component.html',
  styleUrls: ['./mailing.component.scss']
})
export class MailingComponent implements OnInit {
  selection = new SelectionModel<RespondentNotification>(true, []);
  scheduleViolationColumns = ["formName", 'periodName', 'sendDate'];
  scheduleViolations: ScheduleViolation[] = [];
  respondentNotificationColumns = ['select', 'katoCode', 'binIin', 'respondentName', 'email', 'telefonStr'];
  respondentNotifications: RespondentNotification[] = [];
  scheduleViolationLenght = 0;
  scheduleViolationIndex = 0;
  scheduleViolationSize = 5;
  respondentNotificationLength = 0;
  respondentNotificationIndex = 0;
  respondentNotificationSize = 5;
  scheduleViolationId = '';
  showDeletedMode = false;

  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public authService: AuthService,
    public messageService: MessageService,
    public scheduleViolationService: ScheduleViolationService,
    public respondentNotificationService: RespondentNotificationService,
    private snackBar: MatSnackBar
  ) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild("filter", {static: true}) filter: ElementRef;

  ngOnInit(): void {
    this.getScheduleViolationPageable();
  }

  showDeleted() {
    if (this.showDeletedMode) {
      this.showDeletedMode = false;
      this.respondentNotificationLength = 0;
      this.respondentNotificationIndex = 0;
      this.respondentNotificationSize = 5;
      this.getRespondentNotificationsByScheduleViolationId(this.scheduleViolationId);
    } else {
      this.respondentNotificationService.showDeleted(this.scheduleViolationId).subscribe(res => {
        this.showDeletedMode = true;
        this.respondentNotifications = res.content;
        this.respondentNotificationLength = res.totalElements;
        this.respondentNotificationIndex = res.page;
        this.respondentNotificationSize = res.size;
      });
    }
  }

  getScheduleViolationPageable(event?: PageEvent) {
    let params = '';

    if (event) {
      params = 'page=' + event.pageIndex + '&size=' + event.pageSize;
    } else {
      this.scheduleViolationLenght = 0;
      this.scheduleViolationIndex = 0;
      this.scheduleViolationSize = 5;
      params = 'page=' + this.scheduleViolationIndex + '&size=' + this.scheduleViolationSize;
    }
    this.scheduleViolationService.getAllPageable(params).subscribe(res => {
      this.scheduleViolations = res.content;
      this.scheduleViolationIndex = res.page;
      this.scheduleViolationSize = res.size;
      this.scheduleViolationLenght = res.totalElements;
    });
  }

  getRespondentNotificationPageable(event?: PageEvent) {
    let params = '';
    if (event) {
      params = 'page=' + event.pageIndex + '&size=' + event.pageSize + "&svId=" + this.scheduleViolationId;
    } else {
      this.respondentNotificationLength = 0;
      this.respondentNotificationIndex = 0;
      this.respondentNotificationSize = 5;
      params = 'page=' + this.respondentNotificationIndex + '&size=' +
        this.respondentNotificationSize + "&svId=" + this.scheduleViolationId;
    }
    this.respondentNotificationService.getAllPageable(params).subscribe(res => {
      this.respondentNotifications = res.content;
      this.respondentNotificationIndex = res.page;
      this.respondentNotificationSize = res.size;
      this.respondentNotificationLength = res.totalElements;
    });
  }

  deleteOrActivate() {
    let selectedItems = this.selection.selected;
    for (let notify of selectedItems) {
      // @ts-ignore
      notify.updatedBy = this.authService.currentUserValue.username;
    }

    console.log('selection', this.selection.selected);
    if (!this.showDeletedMode) {
      this.respondentNotificationService.deleteAllByIds(selectedItems).subscribe(res => {
        console.log('succesfully deleted:', res);
        this.getRespondentNotificationPageable();
      })
    } else {
      this.respondentNotificationService.activateAllByIds(selectedItems).subscribe(res => {
        console.log('succesfully activated:', res);
        this.respondentNotificationService.showDeleted(this.scheduleViolationId).subscribe(res => {
          this.showDeletedMode = true;
          this.respondentNotifications = res.content;
          this.respondentNotificationIndex = res.page;
          this.respondentNotificationSize = res.size;
          this.respondentNotificationLength = res.totalElements;
        });
      })
    }

  }

  sendMessages() {
    let ids = [];
    this.respondentNotifications.forEach(row => this.selection.isSelected(row) ? ids.push(row.id) : console.log("asd"));
    this.messageService.sendMessage(ids).subscribe(value => {
      console.log(value)
    })
  }

  getRespondentNotificationsByScheduleViolationId(id: string) {
    this.scheduleViolationId = id;
    this.getRespondentNotificationPageable();
  }

  customCut(obj: string, ln: number): string {
    if (obj != null && obj.length > ln) {
      return obj.substring(0, ln) + '......';
    }
    if (obj === null || obj.length === null) {
      return '-';
    }
    return obj;
  }

  removeSelectedRows() {

  }

  addNew() {

  }

  refresh() {

  }

  viewDetail(row) {

  }

  selectDetail(i, row) {

  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.respondentNotifications.forEach((row) =>
        this.selection.select(row)
      );
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.respondentNotifications.length;
    return numSelected === numRows;
  }

  editCall(row) {
    const dialogRef = this.dialog.open(RespondentNotificationFormComponent, {
      data: {
        respondent: row,
        action: "view",
      },
      direction: 'ltr',
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
    });

  }
}

// export class ExampleDataSource extends DataSource<Doctors> {
//   filterChange = new BehaviorSubject("");
//
//   get filter(): string {
//     return this.filterChange.value;
//   }
//
//   set filter(filter: string) {
//     this.filterChange.next(filter);
//   }
//
//   filteredData: Doctors[] = [];
//   renderedData: Doctors[] = [];
//
//   constructor(
//     public exampleDatabase: DoctorsService,
//     public paginator: MatPaginator,
//     public tSort: MatSort
//   ) {
//     super();
//     // Reset to the first page when the user changes the filter.
//     this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
//   }
//
//   /** Connect function called by the table to retrieve one stream containing the data to render. */
//   connect(): Observable<Doctors[]> {
//     // Listen for any changes in the base data, sorting, filtering, or pagination
//     const displayDataChanges = [
//       this.exampleDatabase.dataChange,
//       this.tSort.sortChange,
//       this.filterChange,
//       this.paginator.page,
//     ];
//     this.exampleDatabase.getAllDoctorss();
//     return merge(...displayDataChanges).pipe(
//       map(() => {
//         // Filter data
//         this.filteredData = this.exampleDatabase.data
//           .slice()
//           .filter((doctors: Doctors) => {
//             const searchStr = (
//               doctors.name +
//               doctors.department +
//               doctors.specialization +
//               doctors.degree +
//               doctors.email +
//               doctors.mobile
//             ).toLowerCase();
//             return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
//           });
//         // Sort filtered data
//         const sortedData = this.sortData(this.filteredData.slice());
//         // Grab the page's slice of the filtered sorted data.
//         const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
//         this.renderedData = sortedData.splice(
//           startIndex,
//           this.paginator.pageSize
//         );
//         return this.renderedData;
//       })
//     );
//   }
//
//   disconnect() {
//   }
//
//   /** Returns a sorted copy of the database data. */
//   sortData(data: Doctors[]): Doctors[] {
//     if (!this.tSort.active || this.tSort.direction === "") {
//       return data;
//     }
//     return data.sort((a, b) => {
//       let propertyA: number | string = "";
//       let propertyB: number | string = "";
//       switch (this.tSort.active) {
//         case "id":
//           [propertyA, propertyB] = [a.id, b.id];
//           break;
//         case "name":
//           [propertyA, propertyB] = [a.name, b.name];
//           break;
//         case "email":
//           [propertyA, propertyB] = [a.email, b.email];
//           break;
//         case "date":
//           [propertyA, propertyB] = [a.date, b.date];
//           break;
//         case "time":
//           [propertyA, propertyB] = [a.department, b.department];
//           break;
//         case "mobile":
//           [propertyA, propertyB] = [a.mobile, b.mobile];
//           break;
//       }
//       const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
//       const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
//       return (
//         (valueA < valueB ? -1 : 1) * (this.tSort.direction === "asc" ? 1 : -1)
//       );
//     });
//   }
// }
