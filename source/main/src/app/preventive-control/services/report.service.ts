import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ReportGenerationModel} from "../report/report-generation.model";

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  constructor(private http: HttpClient) {
  }

  public generateMailingStatusReport(reportGenerationModel: ReportGenerationModel): Observable<any> {

    return this.http.post("/preventive-control/api/v1/report/mailing/status", reportGenerationModel, {
      responseType: "arraybuffer"
    });
  }
}
