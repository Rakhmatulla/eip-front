import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RespondentNotificationService {

  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<any> {
    return this.http.get("/preventive-control/api/respondent/notification/get/all")
  }

  public getAllPageable(params?: string): Observable<any> {
    if (!params || params != 'undefined')
      return this.http.get("/preventive-control/api/respondent/notification/get/all/pageable?" + params)
    else
      return this.http.get("/preventive-control/api/respondent/notification/get/all/pageable")

  }

  public getById(id: string): Observable<any> {
    return this.http.get("/preventive-control/api/respondent/notification/get/all/" + id)
  }

  public getByIdAndBinIin(id: string, binIin: string): Observable<any> {
    return this.http.get("/preventive-control/api/respondent/notification/get/all/schedule/violation/" + id + "?binIin=" + binIin)
  }


  public deleteAllByIds(ids: any[]): Observable<any> {
    return this.http.post("/preventive-control/api/respondent/notification/deactivate/all", ids);
  }

  showDeleted(svId: string): Observable<any> {
    return this.http.get("/preventive-control/api/respondent/notification/show/deleted/" + svId);
  }

  showSent(svId: string): Observable<any> {
    return this.http.get("/preventive-control/api/respondent/notification/show/sent/" + svId);
  }

  activateAllByIds(selected: any[]): Observable<any> {
    return this.http.post("/preventive-control/api/respondent/notification/activate/all", selected);
  }
}
