import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FileService {
  constructor(private http: HttpClient) {
  }

  public generate(head: string, body: string): Observable<any> {
    return this.http.get("/notification-service/message/body?head=" + head + "&body=" + body, {responseType: "text"});
  }

  downloadPdf(base64String: string, fileName: string): void {
    const source = `data:application/pdf;base64,${base64String}`;
    const link = document.createElement('a');
    link.href = source;
    link.download = `${fileName}.pdf`;
    link.click();
  }

}
