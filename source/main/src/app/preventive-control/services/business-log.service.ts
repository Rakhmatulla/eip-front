import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BusinessLogService {

  constructor(private http: HttpClient) {
  }

  getAllLogsPageable(params: any): Observable<any> {
    return this.http.get("/preventive-control/api/v1/business/log/pageable?" + params);
  }
}
