import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ScheduleViolationService {

  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<any> {
    return this.http.get("/preventive-control/api/schedule/violation/get/all")
  }

  public getAllPageable(params?: string): Observable<any> {
    return this.http.get("/preventive-control/api/schedule/violation/get/all/pageable?" + params)
  }

  public getById(id: string): Observable<any> {
    return this.http.get("/preventive-control/api/schedule/violation/get/" + id)
  }

}
