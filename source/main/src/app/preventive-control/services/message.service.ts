import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {MessageBody} from "../model/messageBody";

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) {
  }

  public sendMessage(ids: string[]): Observable<any> {
    return this.http.post("/notification-service/message/send", ids)
  }

  public generate(message: MessageBody): Observable<any> {

    return this.http.post("/notification-service/message/generate", message, {
      responseType: "text"
    });
  }

  public generateSentFile(svrnId: String): Observable<any> {
    return this.http.get("/notification-service/message/generate/" + svrnId, {
      responseType: "text"
    });
  }

  public getMessageInfo() {
    return this.http.get("/notification-service/message/get");
  }

  public sign(message: string): Observable<any> {
    return this.http.post("/notification-service/message/save", message);
  }

  public getNotification(id: string): Observable<any> {
    return this.http.get("/notification-service/message/get/" + id);
  }

  public getAllByScheduleViolationId(id: string): Observable<any> {
    return this.http.get("/notification-service/message/get/sv/" + id);
  }

}
