import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailingAttachConfigComponent } from './mailing-attach-config.component';

describe('MailingAttachConfigComponent', () => {
  let component: MailingAttachConfigComponent;
  let fixture: ComponentFixture<MailingAttachConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MailingAttachConfigComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MailingAttachConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
