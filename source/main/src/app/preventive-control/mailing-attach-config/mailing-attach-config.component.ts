import { Component, OnInit } from '@angular/core';
import {DynamicVariable} from "../settings/settings.component";

@Component({
  selector: 'app-mailing-attach-config',
  templateUrl: './mailing-attach-config.component.html',
  styleUrls: ['./mailing-attach-config.component.sass']
})
export class MailingAttachConfigComponent implements OnInit {
  dynamicVariables = ELEMENT_DATA;
  displayedColumns: string[] = ['variableName', 'value'];
  constructor() { }

  ngOnInit(): void {
  }

}

const ELEMENT_DATA: DynamicVariable[] = [
  {
    variableName: '%DATE%',
    value: 'Дата',
  }, {
    variableName: '%FIO%',
    value: 'ФИО',
  }, {
    variableName: '%STATUS%',
    value: 'СТАТУС',
  }, {
    variableName: '%BIN%',
    value: 'БИН/ИИН',
  }, {
    variableName: '%PLACE%',
    value: 'Адрес',
  }];
