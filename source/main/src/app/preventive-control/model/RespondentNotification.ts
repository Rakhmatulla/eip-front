export class RespondentNotification {
  id: string;
  cClass: string;
  address: string;
  email: string;
  formName: string;
  katoCode: string;
  periodName: string;
  pklId: number;
  requisiteType: number;
  statusId: string;
  svId: string;
  telefonStr: string;
  status: string;
  desc: string;
  binIin: string;
  typeUnitAcc: string;
  respondentName: string;
  formId: number;
  katoId: number;
  updated: any;
  updatedBy: string | null;
  serviceStatus: string;


  constructor(id: string, cClass: string, address: string, email: string, formName: string, katoCode: string, periodName: string, pklId: number, requisiteType: number, statusId: string, svId: string, telefonStr: string, status: string, desc: string, binIin: string, typeUnitAcc: string, respondentName: string, formId: number, katoId: number, updated: any, updatedBy: string | null, serviceStatus: string) {
    this.id = id;
    this.cClass = cClass;
    this.address = address;
    this.email = email;
    this.formName = formName;
    this.katoCode = katoCode;
    this.periodName = periodName;
    this.pklId = pklId;
    this.requisiteType = requisiteType;
    this.statusId = statusId;
    this.svId = svId;
    this.telefonStr = telefonStr;
    this.status = status;
    this.desc = desc;
    this.binIin = binIin;
    this.typeUnitAcc = typeUnitAcc;
    this.respondentName = respondentName;
    this.formId = formId;
    this.katoId = katoId;
    this.updated = updated;
    this.updatedBy = updatedBy;
    this.serviceStatus = serviceStatus;
  }
}
