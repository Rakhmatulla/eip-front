export class MessageBody {
  id: string;
  head: string;
  headKz: string;
  body: string;
  bodyKz: string;
  subject: string;
  text: string;
  createdDate: Date;


  constructor(id: string, head: string, headKz: string, body: string, bodyKz: string, subject: string, text: string, createdDate: Date) {
    this.id = id;
    this.head = head;
    this.headKz = headKz;
    this.body = body;
    this.bodyKz = bodyKz;
    this.subject = subject;
    this.text = text;
    this.createdDate = createdDate;
  }
}
