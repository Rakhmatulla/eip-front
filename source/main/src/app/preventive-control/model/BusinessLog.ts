import {Timestamp} from "rxjs";

export class BusinessLog {
  section: string;
  actionDate: Timestamp<any>;
  actionOwner: string;
  type: string;
  description: string;


  constructor(section: string, actionDate: Timestamp<any>, actionOwner: string, type: string, description: string) {
    this.section = section;
    this.actionDate = actionDate;
    this.actionOwner = actionOwner;
    this.type = type;
    this.description = description;
  }
}

