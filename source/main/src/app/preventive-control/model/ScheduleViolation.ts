export class ScheduleViolation {
  cClass: string;
  id: string;
  bucket: string;
  formId: number;
  formName: string;
  messageId: string;
  periodName: string;
  pklId: number;
  sendDate: Date;
  statusId: string;
  objectId: string;
  receiver: string;
  sender: string;
  objectType: string;


  constructor(cClass: string, id: string, bucket: string, formId: number, formName: string, messageId: string,
              periodName: string, pklId: number, sendDate: Date, statusId: string, objectId: string,
              receiver: string, sender: string, objectType: string) {
    this.cClass = cClass;
    this.id = id;
    this.bucket = bucket;
    this.formId = formId;
    this.formName = formName;
    this.messageId = messageId;
    this.periodName = periodName;
    this.pklId = pklId;
    this.sendDate = sendDate;
    this.statusId = statusId;
    this.objectId = objectId;
    this.receiver = receiver;
    this.sender = sender;
    this.objectType = objectType;
  }
}
