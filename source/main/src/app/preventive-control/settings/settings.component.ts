import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Editor} from 'ngx-editor';
import {MessageBody} from "../model/MessageBody";
import {MessageService} from "../services/message.service";
import {FormBuilder, FormGroup} from '@angular/forms';

import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  messageBody: MessageBody = new MessageBody('', '', '', '', '', '', '', new Date());
  dynamicVariables = ELEMENT_DATA;
  displayedColumns: string[] = ['variableName', 'value'];





  editorContextId: string;

  public contentLoaded = false;


  constructor(private messageService: MessageService, private _http: HttpClient) {
  }

  ngOnInit(): void {
    this.messageService.getMessageInfo().subscribe(res => {
      console.log(res);
      // @ts-ignore
      this.messageBody = res;
    });
  }


  ngOnDestroy(): void {
  }

}

export interface DynamicVariable {
  variableName: string;
  value: string;
}

const ELEMENT_DATA: DynamicVariable[] = [
  {
    variableName: '%DATE%',
    value: 'Дата',
  }, {
    variableName: '%FIO%',
    value: 'ФИО',
  }, {
    variableName: '%STATUS%',
    value: 'СТАТУС',
  }, {
    variableName: '%BIN%',
    value: 'БИН/ИИН',
  }, {
    variableName: '%PLACE%',
    value: 'Адрес',
  }];
