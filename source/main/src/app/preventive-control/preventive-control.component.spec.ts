import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreventiveControlComponent } from './preventive-control.component';

describe('PreventiveControlComponent', () => {
  let component: PreventiveControlComponent;
  let fixture: ComponentFixture<PreventiveControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreventiveControlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreventiveControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
