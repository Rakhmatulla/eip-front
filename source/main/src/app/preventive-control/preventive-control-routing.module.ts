import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {PreventiveControlComponent} from "./preventive-control.component";

const routes: Routes = [
  {
    path: "",
    component: PreventiveControlComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreventiveControlRoutingModule {}
