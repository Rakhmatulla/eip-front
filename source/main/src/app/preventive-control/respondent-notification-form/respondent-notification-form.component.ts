import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-respondent-notification-form',
  templateUrl: './respondent-notification-form.component.html',
  styleUrls: ['./respondent-notification-form.component.scss']
})
export class RespondentNotificationFormComponent implements OnInit {
  action: string;
  respondent: any;
  constructor(
    public dialogRef: MatDialogRef<RespondentNotificationFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.action = data.action;
    this.respondent = data.respondent;
  }

  ngOnInit(): void {
  }

  getPhoneNumbersList(): string[] {
    return this.respondent.telefonStr.split(',');
  }

}
