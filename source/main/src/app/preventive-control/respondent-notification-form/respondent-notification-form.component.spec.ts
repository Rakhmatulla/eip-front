import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RespondentNotificationFormComponent } from './respondent-notification-form.component';

describe('RespondentNotificationFormComponent', () => {
  let component: RespondentNotificationFormComponent;
  let fixture: ComponentFixture<RespondentNotificationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RespondentNotificationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RespondentNotificationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
