import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {BusinessLog} from "../model/BusinessLog";
import {PageEvent} from "@angular/material/paginator";
import {BusinessLogService} from "../services/business-log.service";

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit {
  username: string | null = '';
  dateBefore: Date | null = null;
  dateAfter: Date | null = null;
  businessLogColumns = ['section', 'actionDate', 'actionOwner', 'type', 'description'];
  businessLogs: BusinessLog[] = [];
  businessLogRows = new Set<BusinessLog>();
  pageIndex = 0;
  pageSize = 5;
  pageLength = 0;


  constructor(private businessLogService: BusinessLogService) {
  }


  ngOnInit(): void {
    this.getAllLogs();
  }

  getAllLogs(event?: PageEvent) {
    this.pageIndex = 0;
    this.pageSize = 5;
    let params = '';
    if (event) {
      params = 'page=' + event.pageIndex + '&size=' + event.pageSize;
    } else {
      params = 'page=' + this.pageIndex + '&size=' + this.pageSize;
    }
    if (this.username.length > 0) {
      params = params + "&username=" + this.username;
    }
    if (this.dateBefore != null) {
      params = params + "&before=" + this.dateBefore.toLocaleString();
    }
    if (this.dateAfter != null) {
      params = params + "&after=" + this.dateAfter.toLocaleString();
    }

    this.businessLogService.getAllLogsPageable(params).subscribe(res => {
      console.log(res);
      this.businessLogs = res.content;
      this.pageIndex = res.number;
      this.pageSize = res.size;
      this.pageLength = res.totalElements;
    });
  }

  // applyFilter(event: Event) {
  //   const filterValue = (event.target as HTMLInputElement).value;
  //   this.histArray.filter = filterValue.trim().toLowerCase();
  //
  //   if (this.histArray.paginator) {
  //     this.histArray.paginator.firstPage();
  //   }
  // }
  //
  // ngAfterViewInit() {
  //   this.histArray.paginator = this.paginator;
  //   this.histArray.sort = this.sort;
  // }

}
