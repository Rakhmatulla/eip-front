import { Role } from "./role";

export class User {
  img: string;
  username: string;
  roles: string;
  token: string;
}
