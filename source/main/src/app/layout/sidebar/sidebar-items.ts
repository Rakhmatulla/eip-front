import { RouteInfo } from "./sidebar.metadata";
export const ROUTES: RouteInfo[] = [
  {
    path: "prev-control",
    title: "Preventive control",
    moduleName: "prev-control",
    iconType: "material-icons-two-tone",
    icon: "event_note",
    class: "",
    groupTitle: false,
    badge: "",
    badgeClass: "badge bg-blue sidebar-badge float-end",
    role: ["ROLE_ESTATVIP_PREV_CONTROL_ADMIN", "ROLE_ESTATVIP_VIEW+ESTATVIP_PREV_CONTROL_EDIT+ESTAT00"],
    submenu: [],
  }
];
